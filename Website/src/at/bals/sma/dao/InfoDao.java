package at.bals.sma.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.websocket.server.PathParam;

import at.bals.sma.infoObject.Info;

public class InfoDao {
	private static final String ipAddress = "172.16.19.139";
	private static final String databaseName = "laenderApp";
	private static final String userName = "firstRest";
	private static final String password = "lukibals";
	private Connection connection;

	/**
	 * Returns all infos from the Database
	 * 
	 * @return
	 * @throws SQLException
	 */
	public List<Info> getData() throws SQLException {
		List<Info> infoList = new ArrayList<Info>();
		if (this.connection == null || this.connection.isClosed()) {
			setConnection();
		}

		Statement stmt = this.connection.createStatement();
		ResultSet resultSet = stmt.executeQuery("select * from laender;");
		resultSet.first();
		while (!(resultSet.isAfterLast())) {
			Info i = new Info(resultSet.getInt(1), resultSet.getString(2),
					resultSet.getString(3));
			infoList.add(i);
			resultSet.next();
		}
		stmt.close();
		connection.close();
		return infoList;
	}

	/**
	 * Returns one Info by id
	 * 
	 * @return
	 * @throws SQLException
	 */
	public Info getDataById(@PathParam("id") int id) throws SQLException {
		Info i = null;
		if (this.connection == null || this.connection.isClosed()) {
			setConnection();
		}

		Statement stmt = this.connection.createStatement();
		ResultSet resultSet = stmt
				.executeQuery("select * from laender where id=" + id);
		resultSet.first();
		if (!(resultSet.isAfterLast())) {
			i = new Info(resultSet.getInt(1), resultSet.getString(2),
					resultSet.getString(3));
		}
		stmt.close();
		connection.close();
		return i;
	}

	/**
	 * Inserts one Info
	 * 
	 * @return
	 * @throws SQLException
	 */
	public void insertData(Info i) throws SQLException {
		if (this.connection == null || this.connection.isClosed()) {
			setConnection();
		}
		Statement stmt = this.connection.createStatement();
		stmt.executeUpdate("insert into firstRest.firstRest (name, info) VALUES ('"
				+ i.getName() + "', '" + i.getInfo() + "');");
		stmt.close();
		connection.close();
	}

	/**
	 * Deletes one Info
	 * 
	 * @return
	 * @throws SQLException
	 */
	public void deleteData(Integer id) throws SQLException {
		if (this.connection == null || this.connection.isClosed()) {
			setConnection();
		}
		Statement stmt = this.connection.createStatement();
		stmt.executeUpdate("delete from firstRest where id=" + id + ";");
		stmt.close();
		connection.close();
	}

	/**
	 * Updates one Info
	 * 
	 * @return
	 * @throws SQLException
	 */
	public void updateData(Info i, Integer id) throws SQLException {
		if (this.connection == null || this.connection.isClosed()) {
			setConnection();
		}
		Statement stmt = this.connection.createStatement();
		stmt.executeUpdate("update firstRest set name='" + i.getName()
				+ "', beschreibung='" + i.getInfo() + "' where id=" + i.getId()
				+ ";");
		stmt.close();
		connection.close();
	}

	/**
	 * Sets a connection to the database
	 */
	private void setConnection() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			this.connection = DriverManager.getConnection("jdbc:mysql://"
					+ ipAddress + "/" + databaseName + "?user=" + userName
					+ "&password=" + password + "");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
