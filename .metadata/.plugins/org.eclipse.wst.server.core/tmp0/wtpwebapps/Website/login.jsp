<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ReiseApp</title>
<script src="./res/js/jquery-1.11.1.min.js"></script>
<script src="./res/js/jquery-ui.min.js"></script>
<script src="./res/js/bootstrap.min.js"></script>
<link href="./res/css/bootstrap.min.css" rel="stylesheet">
<link href="./res/css/jquery-ui.min.css" rel="stylesheet">
<link href="./res/css/ourCss.css" rel="stylesheet">
<script>
	$(function() {
		var isNewOpen = true;
		$("#btnNew").click(function() {
			if (isNewOpen == true) {
				$(".jumboRow").slideToggle();
				$("#btnNew").html("Konto erstellen ausblenden ...")
				isNewOpen = false;
			} else {
				$(".jumboRow").slideToggle();
				$("#btnNew").html("Konto erstellen ...")
				isNewOpen = true;
			}
		});
	});
</script>
</head>
<body>
	<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
	<div class="container">
		<div style="margin-left: 10px;" class="navbar-header">
			<a class="navbar-brand" href="./index.jsp"><span
				class="glyphicon glyphicon-home" aria-hidden="true"></span></a>
		</div>
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
				<li class="active"><a href="./login.jsp">Mein Profil</a></li>
			</ul>
			<ul class="nav navbar-nav">
				<li><a href="./listTop5.jsp">Liste</a></li>
			</ul>
			<ul style="margin-right: 10px;" class="nav navbar-nav navbar-right">

				<li class="dropdown"><a href="#" class="dropdown-toggle"
					data-toggle="dropdown" role="button" aria-expanded="false">Sprache
						<span class="caret"></span>
				</a>
					<ul class="dropdown-menu" role="menu">
						<li class="active"><a href="./index.jsp">Deutsch</a></li>
						<li><a href="./indexEnglisch.jsp">Englisch</a></li>
					</ul></li>
			</ul>
		</div>
		<!--/.nav-collapse -->
	</div>
	</nav>
	<div class="container topObject">
		<div class="row">
			<div class="col-xs-9">
				<h1>Willkommen bei LänderApp</h1>
			</div>
			<div class="col-xs-3">
				<img class="selfIMG" src="./res/img/loginIMG.png" alt="...">
			</div>
		</div>
		<div class="jumbotron jumboTop">
			<div class="row">
				<div class="col-md-6" id="jumboBackground">
					<h2>Melden sie sich an</h2>
					<p>Um Favoriten zu speichern, oder Daten zu einzelnen Ländern
						aufs Smartphone zu laden, melden sie sich an. Wenn sie noch nicht
						angemolden sind, können sie das hier:</p>
					<button style="margin-bottom: 20px;" id="btnNew"
						class="btn btn-md btn-default btn-block" type="submit">Konto
						erstellen ...</button>
				</div>
				<div class="col-md-6 jumboCenter">
					<form class="form-signin">
						<h2 class="form-signin-heading">Hier einloggen</h2>
						<div class="input-group">
							<span class="input-group-addon" id="basic-addon1">eMail</span> <input
								type="text" class="form-control"
								placeholder="E - Mail eingeben ..."
								aria-describedby="basic-addon1">
						</div>
						<p />
						<div class="input-group">
							<span class="input-group-addon" id="basic-addon1">Secret</span> <input
								type="text" class="form-control"
								placeholder="Passwort eingeben ..."
								aria-describedby="basic-addon1">
						</div>
						<button id="jumboButton" class="btn btn-md btn-primary btn-block"
							type="submit">Einloggen</button>
					</form>
				</div>
			</div>
			<div class="row jumboRow">
				<div class="col-md-12">
					<h2>Konto erstellen</h2>
				</div>
			</div>
			<div class="row jumboRow jumboRowSpace">
				<div class="col-md-12">
					<strong>Name</strong>
				</div>
			</div>
			<div class="row jumboRow">
				<div class="col-md-4">
					<p class="text-right">Vorname</p>
				</div>
				<div class="col-md-5">
					<input type="text" class="form-control" placeholder="Vorname ..."
						aria-describedby="basic-addon1">
				</div>
			</div>
			<div class="row jumboRow">
				<div class="col-md-4">
					<p class="text-right">Nachname</p>
				</div>
				<div class="col-md-5">

					<input type="text" class="form-control" placeholder="Nachname ..."
						aria-describedby="basic-addon1">
				</div>
			</div>
			<div class="row jumboRow">
				<div class="col-md-4">
					<p class="text-right">Username</p>
				</div>
				<div class="col-md-5">

					<input type="text" class="form-control"
						placeholder="Namen eingeben ..." aria-describedby="basic-addon1">
				</div>
			</div>
			<div class="row jumboRow jumboRowSpace">
				<div class="col-md-12">
					<strong>Passwort</strong>
					<p>Geben sie ein Passwort ein, welches sie für ReiseApp
						verwenden wollen.</p>
				</div>
			</div>
			<div class="row jumboRow">
				<div class="col-md-4">
					<p class="text-right">Passwort</p>
				</div>
				<div class="col-md-5">

					<input type="password" class="form-control"
						placeholder="Passwort eingeben ..."
						aria-describedby="basic-addon1">
				</div>
			</div>
			<div class="row jumboRow">
				<div class="col-md-4">
					<p class="text-right">PW bestätigen</p>
				</div>
				<div class="col-md-5">

					<input type="password" class="form-control"
						placeholder="Passwort bestätigen ..."
						aria-describedby="basic-addon1">
				</div>
			</div>
			<div class="row jumboRow">
				<div class="col-md-4"></div>
				<div class="col-md-5">
					<button id="jumboButton" class="btn btn-md btn-primary btn-block"
						type="submit">Speichern</button>
				</div>
			</div>
		</div>
		<ol class="breadcrumb">
			<li><a href="./index.jsp">Home</a></li>
			<li class="active">Login</li>
		</ol>
	</div>
</body>
</html>