<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ReiseApp</title>
<script src="./res/js/jquery-1.11.1.min.js"></script>
<script src="./res/js/jquery-ui.min.js"></script>
<script src="./res/js/bootstrap.min.js"></script>
<link href="./res/css/bootstrap.min.css" rel="stylesheet">
<link href="./res/css/jquery-ui.min.css" rel="stylesheet">
<link href="./res/css/ourCss.css" rel="stylesheet">
<script>
$(function(){
	//Die id des gewünschten Landes hier einfügen um es bei den Slides erscheinen zu lassen
	var idFirstSlide = 5;
	var idSecondSlide = 1;
	var idThirdSlide = 4;
	
	loadDataFirstSlide(idFirstSlide);
	loadDataSecondSlide(idSecondSlide);
	loadDataThirdSlide(idThirdSlide);
	
	function loadDataFirstSlide(idFirstSlide) {
		$.ajax({
			headers : {
				Accept : 'application/json'
			},
			type : 'GET',
			url : '<%=request.getContextPath()%>/rest/infos/'+ idFirstSlide,
				success : function(i) {
					$('#firstSlide').html(i.name);
					$('#contentFirstSlide').html(i.info);
				}
			});
		}
	
	function loadDataSecondSlide(idSecondSlide) {
		$.ajax({
			headers : {
				Accept : 'application/json'
			},
			type : 'GET',
			url : '<%=request.getContextPath()%>/rest/infos/'+ idSecondSlide,
				success : function(i) {
					$('#secondSlide').html(i.name);
					$('#contentSecondSlide').html(i.info);
				}
			});
		}
	
	function loadDataThirdSlide(idThirdSlide) {
		$.ajax({
			headers : {
				Accept : 'application/json'
			},
			type : 'GET',
			url : '<%=request.getContextPath()%>/rest/infos/' + idThirdSlide,
				success : function(i) {
					$('#thirdSlide').html(i.name);
					$('#contentThirdSlide').html(i.info);
				}
			});
		}
	});
</script>
</head>
<body>
	<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
	<div class="container">
		<div style="margin-left: 10px;" class="navbar-header">
			<a class="navbar-brand" href="./index.jsp"><span
				class="glyphicon glyphicon-home" aria-hidden="true"></span></a>
		</div>
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
				<li><a href="./login.jsp">Mein Profil</a></li>
			</ul>
			<ul class="nav navbar-nav">
				<li><a href="./listTop5.jsp">Liste</a></li>
			</ul>
			<ul style="margin-right: 10px;" class="nav navbar-nav navbar-right">
				<li class="dropdown"><a href="#" class="dropdown-toggle"
					data-toggle="dropdown" role="button" aria-expanded="false">Sprache
						<span class="caret"></span>
				</a>
					<ul class="dropdown-menu" role="menu">
						<li class="active"><a href="./index.jsp">Deutsch</a></li>
						<li><a href="./indexEnglisch.jsp">Englisch</a></li>
					</ul></li>
			</ul>
		</div>
	</div>
	</nav>
	<div class="container topObject">
		<div class="row">
			<div id="heading" class="col-md-12">
				<h1 id="bigHeading">Willst du reisen?</h1>
				<h2 id="smallHeading">Die LänderApp hilft dir dabei!</h2>
				<span id="glyphicon" class="glyphicon glyphicon-chevron-down"></span>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div id="carousel-example-generic" class="carousel slide"
				data-ride="carousel">
				<ol class="carousel-indicators">
					<li data-target="#carousel-example-generic" data-slide-to="0"
						class=""></li>
					<li data-target="#carousel-example-generic" data-slide-to="1"
						class="active"></li>
					<li data-target="#carousel-example-generic" data-slide-to="2"
						class=""></li>
				</ol>
				<div class="carousel-inner" role="listbox">
					<div class="item">
						<img class="selfIMG" src="./res/img/collage/spanien.jpg" alt="...">
						<div class="carousel-caption">
							<p>
								<a href="#" id="firstSlide" class="btn btn-md btn-default">Fehler</a>
							</p>
							<p id="contentFirstSlide">
							Fehler
							</p>
						</div>
					</div>

					<div class="item active">
						<img class="selfIMG" src="./res/img/collage/oesterreich.jpg"
							alt="...">
						<div class="carousel-caption">
							<p>
								<a href="#" id="secondSlide" class="btn btn-md btn-default">Fehler</a>
							</p>
							<p id="contentSecondSlide">
							Fehler
							</p>
						</div>
					</div>
					<div class="item">
						<img class="selfIMG" src="./res/img/collage/frankreich.jpg"
							alt="...">
						<div class="carousel-caption">
							<p>
								<a href="#" id="thirdSlide" class="btn btn-md btn-default">Fehler</a>
							</p>
							<p id="contentThirdSlide">
							Fehler
							</p>
						</div>
					</div>
				</div>
				<a class="left carousel-control" href="#carousel-example-generic"
					role="button" data-slide="prev"> <span
					class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
					<span class="sr-only">Previous</span>
				</a> <a class="right carousel-control" href="#carousel-example-generic"
					role="button" data-slide="next"> <span
					class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
					<span class="sr-only">Next</span>
				</a>
			</div>
		</div>
	</div>
	<div class="container">
		<div id="footer">
			<div class="row bottomRow">
				<div class="col-xs-6 col-sm-4">
					<h2 class="center">Kontakt</h2>
					<p class="center">-- eMail --</p>
					<p class="center">
						<a class="linkSize" href="mailto:info@laenderApp.at">info@laenderApp.at</a>
					</p>
					<p class="center">-- Telefon --</p>
					<p class="center">
						<a class="linkSize" href="#">+43 664 9423001</a>
					</p>
				</div>
				<!--/.col-xs-6.col-lg-4-->
				<div class="col-xs-6 col-sm-4">
					<h2 class="center">Entwickelt von</h2>
					<p class="center">Der Auftrag für die Entwicklung kam von
						unserem Lehrer und wir, die 7bWI setzten den Auftrag um.</p>
					<p class="detailsFooter">
						<a class="btn btn-link btn-xs" href="./team.jsp" role="button">View
							details »</a>
					</p>
				</div>
				<!--/.col-xs-6.col-lg-4-->
				<div class="col-xs-6 col-sm-4">
					<h2 class="center">Gemacht für</h2>
					<p class="center">Der Auftrag zur Programmierung dieser
						Anwendung kam von unserem Projektmanagement-Lehrer Markus
						Schwärzler. Er wollte mit seiner Frau verreisen und wusste nicht
						wo hin.</p>
				</div>
			</div>
		</div>
	</div>
</body>
</html>