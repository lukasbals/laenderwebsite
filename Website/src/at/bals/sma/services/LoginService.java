package at.bals.sma.services;

import java.sql.SQLException;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import at.bals.sma.dao.LoginDao;
import at.bals.sma.infoObject.Login;

@Path("login")
public class LoginService {
	//private List<Login> loginList = new ArrayList<Login>();

	@POST
	@Path("")
	// @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Response addTask(Login l) {
		try {
			LoginDao dao = new LoginDao();
			dao.insertData(l);
			return Response.status(201).build();
		} catch (SQLException e) {
			return Response.status(400).build();
		}
	}
}
