<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ReiseApp</title>
<script src="./res/js/jquery-1.11.1.min.js"></script>
<script src="./res/js/jquery-ui.min.js"></script>
<script src="./res/js/bootstrap.min.js"></script>
<link href="./res/css/bootstrap.min.css" rel="stylesheet">
<link href="./res/css/jquery-ui.min.css" rel="stylesheet">
<link href="./res/css/ourCss.css" rel="stylesheet">
<script>
	$(function() {
		$("#searchClick").click(function() {
			$("#searchToggle").fadeToggle();
		});
	});
</script>
</head>
<body>
	<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
	<div class="container">
		<div style="margin-left: 10px;" class="navbar-header">
			<a class="navbar-brand" href="./index.jsp"><span
				class="glyphicon glyphicon-home" aria-hidden="true"></span></a>
		</div>
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
				<li><a href="./login.jsp">Mein Profil</a></li>
			</ul>
			<ul class="nav navbar-nav">
				<li class="active"><a href="./listTop5">Liste</a></li>
			</ul>
			<ul style="margin-right: 10px;" class="nav navbar-nav navbar-right">
				<li>
					<form class="navbar-form navbar-right">
						<input id="searchToggle" type="text" class="form-control"
							placeholder="Search...">
					</form>
				</li>
				<li><a class="cursor" id="searchClick"><span
						class="glyphicon glyphicon-search" aria-hidden="true"></span></a></li>
				<li class="dropdown"><a href="#" class="dropdown-toggle"
					data-toggle="dropdown" role="button" aria-expanded="false">Sprache
						<span class="caret"></span>
				</a>
					<ul class="dropdown-menu" role="menu">
						<li class="active"><a href="./index.jsp">Deutsch</a></li>
						<li><a href="./indexEnglisch.jsp">Englisch</a></li>
					</ul></li>

			</ul>
		</div>
	</div>
	</nav>
	<div class="container topObject">
		<div class="row">
			<ul class="nav nav-tabs">
				<li role="presentation" class="navTab50"><a class="navTab"
					href="./listTop5.jsp">Top 5</a></li>
				<li role="presentation" class="navTab50 active"><a
					class="navTab" href="./listAlle.jsp">Alle</a></li>
			</ul>
		</div>
		<div class="row selfRow">
			<div class="col-sm-6">
				<img class="selfIMG" src="./res/img/laenderBilder/newyork.jpg" alt="...">
			</div>
			<div class="col-sm-6">
				<h2 class="liste">New York</h2>
				<p class="liste">Info ...</p>
			</div>
		</div>
	</div>
	<div class="darkBackground">
		<div class="container">
			<div class="row selfRow">
				<div class="col-sm-6">
					<h2 class="liste">Paris</h2>
					<p class="liste">Info ...</p>
				</div>
				<div class="col-sm-6">
					<img class="selfIMG" src="./res/img/laenderBilder/paris.jpg" alt="...">
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row selfRow">
			<div class="col-sm-6">
				<img class="selfIMG" src="./res/img/laenderBilder/newyork.jpg" alt="...">
			</div>

			<div class="col-sm-6">
				<h2 class="liste">New York</h2>
				<p class="liste">Info ...</p>
			</div>
		</div>
	</div>
	<div class="darkBackground">
		<div class="container">
			<div class="row selfRow">
				<div class="col-sm-6">
					<h2 class="liste">Paris</h2>
					<p class="liste">Info ...</p>
				</div>
				<div class="col-sm-6">
					<img class="selfIMG" src="./res/img/laenderBilder/paris.jpg" alt="...">
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row selfRow">
			<div class="col-sm-6">
				<img class="selfIMG" src="./res/img/laenderBilder/newyork.jpg" alt="...">
			</div>

			<div class="col-sm-6">
				<h2 class="liste">New York</h2>
				<p class="liste">Info ...</p>
			</div>
		</div>
	</div>
	<div class="container">
		<ol class="breadcrumb">
			<li><a href="./index.jsp">Home</a></li>
			<li class="active">Liste</li>
		</ol>
	</div>
</body>
</html>