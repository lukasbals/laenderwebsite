<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ReiseApp</title>
<script src="./res/js/jquery-1.11.1.min.js"></script>
<script src="./res/js/jquery-ui.min.js"></script>
<script src="./res/js/bootstrap.min.js"></script>
<link href="./res/css/bootstrap.min.css" rel="stylesheet">
<link href="./res/css/jquery-ui.min.css" rel="stylesheet">
<link href="./res/css/ourCss.css" rel="stylesheet">
</head>
<body>
	<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
	<div class="container">
		<div style="margin-left: 10px;" class="navbar-header">
			<a class="navbar-brand" href="#">Icon</a>
		</div>
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
				<li><a href="#">My profile</a></li>
			</ul>
			<ul style="margin-right: 10px;" class="nav navbar-nav navbar-right">

				<li class="dropdown"><a href="#" class="dropdown-toggle"
					data-toggle="dropdown" role="button" aria-expanded="false">Language
						<span class="caret"></span>
				</a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="./index.jsp">Deutsch</a></li>
						<li class="active"><a href="./indexEnglisch.jsp">Englisch</a></li>
					</ul></li>
			</ul>
		</div>
	</div>
	</nav>

	<div class="container">
		<div class="row">
			<div id="heading" class="col-md-12">
				<h1 id="bigHeading">Do you want to travel?</h1>
				<h2 id="smallHeading">LänderApp will help you!</h2>
				<span id="glyphicon" class="glyphicon glyphicon-chevron-down"></span>
			</div>
		</div>

	</div>

	<div class="row">
		<div class="col-md-12">
			<img class="selfIMG" src="./res/img/collage.jpg" alt="...">

		</div>
	</div>
	<div class="row">
		<div class="page-header">
			<h1>Best 10</h1>
		</div>
	</div>
	<div class="container">
		<div class="row selfRow">
			<div class="col-md-6">
				<img class="selfIMG" src="./res/img/newyork.jpg" alt="...">
			</div>

			<div class="col-md-6">
				<h1>New York</h1>
				<h2>INFO</h2>
			</div>
		</div>
	</div>
	<div class="darkBackground">
		<div class="container">
			<div class="row selfRow">
				<div class="col-md-6">
					<h1>Paris</h1>
					<h2>INFO</h2>
				</div>

				<div class="col-md-6">
					<img class="selfIMG" src="./res/img/paris.jpg" alt="...">
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row selfRow">
			<div class="col-md-6">
				<img class="selfIMG" src="./res/img/newyork.jpg" alt="...">
			</div>

			<div class="col-md-6">
				<h1>New York</h1>
				<h2>INFO</h2>
			</div>
		</div>
	</div>
	<div class="darkBackground">
		<div class="container">
			<div class="row selfRow">
				<div class="col-md-6">
					<h1>Paris</h1>
					<h2>INFO</h2>
				</div>

				<div class="col-md-6">
					<img class="selfIMG" src="./res/img/paris.jpg" alt="...">
				</div>
			</div>
		</div>
	</div>
	<div id="footer">
		<div class="row">
			<div class="container bottomRow">
				<div class="col-xs-6 col-lg-4">
					<h2>Contact ...</h2>
					<p class="detailsFooter">Englischa Text ...</p>
					<p class="detailsFooter">
						<a class="btn btn-primary btn-xs" href="#" role="button">View
							details »</a>
					</p>
				</div>
				<!--/.col-xs-6.col-lg-4-->
				<div class="col-xs-6 col-lg-4">
					<h2>Developed by ...</h2>
					<p class="detailsFooter">Englischa Text ...</p>
					<p class="detailsFooter">
						<a class="btn btn-primary btn-xs" href="#" role="button">View
							details »</a>
					</p>
				</div>
				<!--/.col-xs-6.col-lg-4-->
				<div class="col-xs-6 col-lg-4">
					<h2>Built for ...</h2>
					<p class="detailsFooter">Wirentwickelten diese Applikation um
						...</p>
					<p class="detailsFooter">
						<a class="btn btn-primary btn-xs" href="#" role="button">View
							details »</a>
					</p>
				</div>
			</div>
		</div>
	</div>

</body>
</html>