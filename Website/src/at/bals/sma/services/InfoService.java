package at.bals.sma.services;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import at.bals.sma.dao.InfoDao;
import at.bals.sma.infoObject.Info;

@Path("infos")
public class InfoService {
	// private Connection connection;
	private List<Info> infoList = new ArrayList<Info>();

	@GET
	@Path("")
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public List<Info> getAllTasks() {
		infoList.clear();
		try {
			InfoDao dao = new InfoDao();
			this.infoList = dao.getData();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return infoList;
	}
	
	@GET
	@Path("{id}/")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Info getInfoById(@PathParam("id") int id){
		InfoDao dao = new InfoDao();
		Info i = null;
		try {
			i = dao.getDataById(id);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return i;
	}

	@POST
	@Path("")
	// @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Response addTask(Info info) {
		try {
			InfoDao dao = new InfoDao();
			dao.insertData(info);
			return Response.status(201).build();
		} catch (SQLException e) {
			return Response.status(400).build();
		}
	}

	@DELETE
	@Path("{id}/")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Response deleteTask(@PathParam("id") int id) {
		try {
			InfoDao dao = new InfoDao();
			dao.deleteData(id);
			return Response.status(200).build();
		} catch (SQLException e) {
			return Response.status(400).build();
		}
	}

	@PUT
	@Path("{id}/")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Response updateTask(Info info, @PathParam("id") int id) {
		try {
			InfoDao dao = new InfoDao();
			info.setId(id);
			dao.updateData(info, id);
			return Response.status(200).build();
		} catch (SQLException e) {
			return Response.status(400).build();
		}
	}
}
