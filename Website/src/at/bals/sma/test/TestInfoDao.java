package at.bals.sma.test;

import static org.junit.Assert.fail;

import java.sql.SQLException;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import at.bals.sma.dao.InfoDao;
import at.bals.sma.infoObject.Info;

public class TestInfoDao {

	@Test
	public void testGetData() {
		try {
			InfoDao dao = new InfoDao();
			List<Info> infoList = dao.getData();
			if (infoList.size() > 0) {
				Assert.assertTrue(true);
			} else {
				fail("No data");
			}
		} catch (SQLException e) {
			fail(e.getMessage());
		}

	}

	// Test zum holen eines einzelnen Items
	@Test
	public void testGetDataById() {
		InfoDao dao = new InfoDao();
		try {
			Info i = dao.getDataById(1);
			if (i.getName().equals("Austria")) {
				Assert.assertTrue(true);
			} else {
				fail();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testInsertData() {
		try {
			InfoDao dao = new InfoDao();
			Info i = new Info(0, "hans", "hans");
			dao.insertData(i);

			Assert.assertTrue(true);

		} catch (SQLException e) {
			fail(e.getMessage());
		}

	}

	@Test
	public void testDeleteData() {
		try {
			InfoDao dao = new InfoDao();
			int id = 15;
			dao.deleteData(id);

			Assert.assertTrue(true);

		} catch (SQLException e) {
			fail(e.getMessage());
		}

	}

}
